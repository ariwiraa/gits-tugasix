package main

import (
	"errors"
	"fmt"
	"strings"
)

// func validateMenus(menus []string, inputMenu string) (bool, error) {
// 	for _, menu := range menus {
// 		if menu != inputMenu {
// 			return true, errors.New("Tidak ada di dalam menu")
// 		}
// 	}
// 	return false, nil
// }

func endApp() {
	if panicErorr := recover(); panicErorr != nil {
		fmt.Println("Terdapat Error", panicErorr)
	} else {
		fmt.Println("Terima kasih telah memesan")
	}
}

func validate(name string) (bool, error) {
	if strings.TrimSpace(name) == "" {
		return false, errors.New("Tidak boleh kosong")
	}

	return true, nil
}

func main() {
	defer endApp()
	var name string
	menus := []string{"Tahu", "Tempe", "Sambal", "Nasi", "Lele", "Ayam"}
	var orders []string
	var continueMenu string

	fmt.Println("Toko Makanan Indonesia")
	fmt.Println("=======================")
	// fmt.Println("Tahu\nTempe\nSambal\nNasi\nLele\nAyam")

	for _, menu := range menus {
		fmt.Println(menu)
	}

	for {

		fmt.Print("Masukan menu makanan anda dalam huruf (eg: Tahu) :")
		fmt.Scan(&name)

		if valid, err := validate(name); valid {
			orders = append(orders, name)
		} else {
			panic(err.Error())
		}

		fmt.Println("Anda ingin melanjutkan pemesanan: (y/n)")
		fmt.Scan(&continueMenu)

		if continueMenu == "y" {
			continue
		} else if continueMenu == "n" {
			for _, order := range orders {
				fmt.Println("Pesanan anda :", order)
			}
			break
		} else {
			fmt.Println("Tidak ada pilihan tersebut")
		}
	}

}

// if validMenu, err := validateMenus(orders, name); validMenu {
// 	orders = append(orders, name)
// } else {
// 	panic(err.Error())
// }
